# Previewing Markdown and PlantUML in Visual Studio Code

[PlantUML](https://plantuml.com/) lets you create a variety of diagrams in [Markdown](https://en.wikipedia.org/wiki/Markdown) documents.

When I work with Markdown and PlantUML diagrams, I do so in Visual Studio code with the Markdown Preview Enhanced extension and a PlantUML Server running in Docker. As I type, the document is rendered in real time in the right-hand preview pane.

For assignments that use PlantUML, I have added a Visual Studio Code devcontainer that starts a PlantUML server in Docker and installs the Markdown Preview Enhanced extension for Visual Studio Code.

To preview a Markdown document with Plant UML, open the file in Visual Studio Code and open the preview pane with `Crtl+K V` or right-click and choose `Markdown Preview Enhanced: Open Preview to the Side`

Copyright © 2022 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
