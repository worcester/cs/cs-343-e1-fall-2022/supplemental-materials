# Sofware Development Supplemental Materials

- &#128240; indicates a written resource
- &#128250; indicates a video resource
- &#127911; indicates an audio resource

## Code/Design Smells

- &#128240; [Code Smell (a.k.a. Software Design Smells)](https://www.alpharithms.com/code-smell-492316/)
- &#128240; [Top 4 symptoms of bad code](https://www.excella.com/insights/top-4-symptoms-of-bad-code)
- &#128240; [Bad code exposed! Do you really know when your code smells?](https://www.telerik.com/blogs/bad-code-exposed-do-you-really-know-when-your-code-smells)
- &#128240; [Object-oriented design smells](http://khanhpdt.github.io/articles/2016/11/01/oo-design-smells/)
- &#128240; [How to Write Unmaintainable Code](https://github.com/Droogans/unmaintainable-code)
- &#128240; [Code Smells](https://blog.codinghorror.com/code-smells/)

## DevDocs

- &#128240; [DevDocs](https://devdocs.io/) combines multiple API documentations in a fast, organized, and searchable interface.

## Design Patterns

## Docker

- &#128240; [Docker Docs](https://docs.docker.com/)

## Frontends

## Git

- &#128240; [Git from DevDocs](https://devdocs.io/git/)
- &#128240; [Pulling from an upstream repository](./Pulling-from-upstream.md)

## Growth Mindset

- &#128250; [Developing a Growth Mindset with Carol Dweck](https://youtu.be/hiiEeMN7vbQ)
- &#128250; [Growth Mindset Introduction: What it is, How it Works, and Why it Matters](https://youtu.be/75GFzikmRY0)

## HTTP Methods

## Installing Software

- &#128240; [Installing Git, Docker, and Visual Studio Code](./Installing-Git-Docker-VSCode.md)

## Javascript

## Markdown

- &#128240; [Markdown Cheat Sheet](https://devhints.io/markdown)
- &#128240; [Markdown from DevDocs](https://devdocs.io/markdown/)

### PlantUML

- &#128240; [PlantUML Documentation](https://plantuml.com/)
  - &#128240; [Class Diagrams](https://plantuml.com/class-diagram)
- &#128240; [Ashley's PlantUML Documentation](https://plantuml-documentation.readthedocs.io/en/latest/)
- &#128240; [PlantUML Preview in Visual Studio Code](./PlantUML-VSC.md)
- &#128240; [Real World Examples of PlantUML](https://real-world-plantuml.com/)

## Microservice Architecture

## MongoDB

## Object-Oriented Design Principles

- &#128240; [How to explain object-oriented programming concepts to a 6-year-old](https://www.freecodecamp.org/news/object-oriented-programming-concepts-21bb035f7260/)
  - My only complaint about this is that polymorphism doesn't apply *only* to inheritance. Any time two classes implement the same interface it is polymorphism.

## OpenAPI

- &#128240; [OpenAPI Map from The OpenAPI Handyman](https://openapi-map.apihandyman.io/)

## POGIL

- &#128240; [What is POGIL?](https://pogil.org/about-pogil/what-is-pogil)

## REST APIs

- &#128240; [REST API Tutorial](https://www.restapitutorial.com/) - A nice, concise set of materials to help you design good REST APIs.
  - [REST API Quick Tips](https://www.restapitutorial.com/lessons/restquicktips.html) - HTTP verbs, resource naming, response codes
  - [HTTP Methods](https://www.restapitutorial.com/lessons/httpmethods.html) - An excellent table. I may replace the Wikipedia one I've been using with this one. Some good discussion below the table.
  - [REST Resource Naming](https://www.restapitutorial.com/lessons/restfulresourcenaming.html) - Especially good with showing how to use hierarchical resource URIs.
  - [HTTP Status Codes](https://www.restapitutorial.com/httpstatuscodes.html) - A very complete table of all the HTTPS response codes.

## Semantic Versioning

## Specification-/Competency-based Grading

## Technical Debt and Refactoring

- &#128240; [Martin Fowler on Technical Debt](https://martinfowler.com/bliki/TechnicalDebt.html)
- &#128240; [Martin Fowler on Refactoring](https://martinfowler.com/bliki/DefinitionOfRefactoring.html)
- &#128240; [Smells to Refactorings Cheatsheet](https://www.industriallogic.com/blog/smells-to-refactorings-cheatsheet/)

## UML Diagrams

- &#128240; [UML Diagrams](https://www.uml-diagrams.org/) - Good reference.

## Vue

Copyright © 2022 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
