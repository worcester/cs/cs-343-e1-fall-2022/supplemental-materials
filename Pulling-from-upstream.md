# Pulling from Upstream

If a project that you have already forked and cloned makes changes and you want to get them into your fork, you must "pull from upstream".

## Terms

- `upstream` &mdash; The remote repository you forked from.
- `origin` &mdash; The remote repository that you cloned from. Your own fork in this case.
- `local` &mdash; The copy on your local computer.

```plantuml
@startuml
cloud gitlab.com {
    rectangle "The class GitLab group" {
        artifact "The upstream repository" as upstream
    }
    rectangle "Your GitLab account or group" {
        artifact "The origin repository" as origin
    }
}
node "Your computer" {
    artifact "The local repository" as local
}

upstream -[dashed]> origin : fork
origin -[dashed]-> local : clone
upstream --> local : pull
local --> origin : push
@enduml
```

The *fork* and *clone* operations happened previously.

## Goal

Get the changes from the upstream repository into your local clone, and then pushed to your remote (origin) repository.

## Steps

All these `git` commands are given from a terminal open in the directory of your local clone repository.

1. Check where your clone came from: `git remote -v`. The result should be the link for your fork.
2. Set up a new remote for the upstream:

  ```git
  git remote add upstream <the link for the project you forked from>
  ```

  such as `https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/api.git` - get this from the `Clone` button in GitLab.)

<!-- markdownlint-disable MD029 -->
3. Make sure that you have committed any changes in your local repository.
4. Pull the changes from upstream: `git pull upstream main`
5. If there are any conflicts, you will need to resolve them, and then commit the corrected files.
6. Push the changes to your own remote repository: `git push origin main`

Copyright © 2022 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
