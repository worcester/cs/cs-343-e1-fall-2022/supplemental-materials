# Installing Git, Docker, and Visual Studio Code

Once we get these three tools installed on your computer, you will not have to install any other tools during the semester.

These installations are relatively straightforward and documented, but occasionally there are still problems. If you run into problems, ask questions in the `#help` channel in Discord. One of your classmates may be able to help, and I will be able to help.

## Update Your Operating System

Working with an outdated version of your operating system can cause problems when installing current versions of development tools.

Update your operating system to its most recent version.

## Install Git

We will use Git and GitLab to manage our source code in this class. All activities and homework will be posted on GitLab, and will be manipulated locally on your own computer using Git.

[Git download page](https://git-scm.com/downloads)

You may have already installed Git in one of your previous classes. If you have, I suggest you check what its version number is. Open a terminal and enter the command `git --version`. If it is not the most recent version, you may want to install the current version.

If you have not installed and used Git before you may want to watch some of the [videos in the documentation section on the Git website](https://git-scm.com/videos) - in particular [Get Going with Git](https://git-scm.com/video/get-going).

You will want to configure git with your name and email:

``` bash
git config --global user.name "Alex Doe"
git config --global user.email alexdoe@example.com
```

## Install Docker Desktop

We will be using Docker in this class to provide containerized development environments with all the tools we need already installed, so you don't have to install a lot of software on your computer to get going.

[Docker Desktop](https://www.docker.com/)

If you already have Docker Desktop installed on your computer, you should make sure that you have the most recent version.

Start Docker Desktop, and open the Dashboard. You can see the version number in the Dashboard in the lower right corner. Hover your cursor over the version number, and it should let you know if you are up to date.

## Install Visual Studio Code

We will use Visual Studio Code as our development environment in this class because it will let us [develop inside a container](https://code.visualstudio.com/docs/remote/containers) that contains all the tools we need for a particular assignment, without having to install the tools directly onto your computer. I have already built development containers for each assignment with the tools installed.

1. Install Visual Studio Code from the [download page](https://code.visualstudio.com/download)
2. Install the [`Dev Containers` extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

Copyright © 2025 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
